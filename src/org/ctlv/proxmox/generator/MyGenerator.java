package org.ctlv.proxmox.generator;

import static org.ctlv.proxmox.api.Constants.CT_BASE_NAME;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.security.auth.login.LoginException;

import org.ctlv.proxmox.api.Constants;
import org.ctlv.proxmox.api.ProxmoxAPI;
import org.ctlv.proxmox.api.data.LXC;
import org.ctlv.proxmox.api.data.Node;
import org.ctlv.proxmox.myManager.Manager;
import org.json.JSONException;


public class MyGenerator {
	long baseID;
	int lambda;
	String whoAmI = "[Generator]: ";
	Map<String, List<LXC>> myCTsPerServer ;
	ProxmoxAPI api;
	Random rndServer;
	Random rndRAM;
	float memRatioOnServer1;
	float memRatioOnServer2;
	
	int ct_id;
	
	public MyGenerator() throws InterruptedException, LoginException, JSONException, IOException{
		baseID = Constants.CT_BASE_ID;
		lambda = 30;
		
		myCTsPerServer = new HashMap<String, List<LXC>>();

		
		api = new ProxmoxAPI();
		rndServer = new Random(new Date().getTime());
		rndRAM = new Random(new Date().getTime());
		memRatioOnServer1 = Constants.MAX_THRESHOLD;
		memRatioOnServer2 = Constants.MAX_THRESHOLD;
		
		ct_id=0;
		
		Thread th = new Threader();

		th.start();
	}
	
	static Random rndTime = new Random(new Date().getTime());
	public static int getNextEventPeriodic(int period) {
		return period;
	}
	public static int getNextEventUniform(int max) {
		return rndTime.nextInt(max);
	}
	public static int getNextEventExponential(int inv_lambda) {
		float next = (float) (- Math.log(rndTime.nextFloat()) * inv_lambda);
		return (int)next;
	}
	
	private boolean inLCXList(List<LXC> LXClist, long ctName) {
		boolean returnvalue = false;
		for (LXC lxc : LXClist) {
			if (lxc.getName().equals(ctName)) {
				returnvalue = true;
			}
		}
		return returnvalue;
	}
    private boolean isOurs(String name) {
        return name.contains(CT_BASE_NAME);
     }
	
    public float getStatusNode(String nodeName)throws InterruptedException, LoginException, JSONException, IOException{
        Node node = api.getNode(nodeName);
        long memTotalNode = node.getMemory_total();

        List<LXC> myCTs= api.getCTs(nodeName);

        long totalRamUsedByUs = 0;

        for(LXC lxc: myCTs){
            if(isOurs(lxc.getName())){
                totalRamUsedByUs+=lxc.getMem();
            }
        }
        return  totalRamUsedByUs/(float)memTotalNode*100;

    }
//	private boolean checkAvailaible(List<LXC> CTsinServerToCreate, List<LXC> CTsinServerNotToCreate, long ct_nb) {
//		boolean bool = false;
//		if (inLCXList(CTsinServerToCreate, ct_nb)){
//			if (CTsinServerToCreate.get((int) ct_nb).getStatus() == "busy") {
//				bool = false;
//			}
//			
//		}
//				(inLCXList(CTsinServerNotToCreate, (int)ct_nb)));
//		
//		return bool;
//	}

	/**
	 * Threader is the generator loop, executed in parallel of the monitor.
	 * It checks if he's allowed to generate, identity a viable id and generate a new ct on a random server.
	 */
	class Threader extends Thread {
		
		public void run() {
			System.out.println(whoAmI+"running..");



			while (true) {
					try {
						myCTsPerServer.put(Constants.SERVER1, api.getCTs(Constants.SERVER1));
						myCTsPerServer.put(Constants.SERVER2, api.getCTs(Constants.SERVER2));

						System.out.println(whoAmI + "Allowed to generate: " + Constants.IS_GENERATOR_ALLOWED_TO_GENERATE);
						if (Constants.IS_GENERATOR_ALLOWED_TO_GENERATE) {
							System.out.println(whoAmI + "Generator current id is : " + ct_id);

							// choisir un serveur al�atoirement avec les ratios sp�cifi�s 66% vs 33%
							String serverNameToCreateCT;
							String serverNameNotToCreateCT;

							if (rndServer.nextFloat() < Constants.CT_CREATION_RATIO_ON_SERVER1) {
								serverNameToCreateCT = Constants.SERVER1;
								serverNameNotToCreateCT = Constants.SERVER2;
							} else {
								serverNameToCreateCT = Constants.SERVER2;
								serverNameNotToCreateCT = Constants.SERVER1;
							}
							// cr�er un contenaire sur ce serveur
							// ...
							ct_id = ct_id + 1;
							ct_id = ct_id % 100;

							long ct_nb = Constants.CT_BASE_ID + ct_id;

							int not_over_100 = 1;

							List<LXC> CTsinServerToCreate = myCTsPerServer.get(serverNameToCreateCT);
							List<LXC> CTsinServerNotToCreate = myCTsPerServer.get(serverNameNotToCreateCT);
							System.out.println(whoAmI+ "Finding a good ct_id");
							while ((not_over_100 < 100)
									&& (inLCXList(CTsinServerToCreate, ct_nb))
									&& (inLCXList(CTsinServerNotToCreate, ct_nb))) {
								//							&& CTsinServerToCreate.get((int) ct_nb).getStatus() == "busy" && (inLCXList(CTsinServerNotToCreate, (int)ct_nb)))) {
								ct_id = ct_id + 1;
								ct_id = ct_id % 100;
								not_over_100 = not_over_100 + 1;
								ct_nb = Constants.CT_BASE_ID + ct_id;
							}

							String ct_name = Constants.CT_BASE_NAME + ct_nb;


							if (not_over_100 < 100) {
								System.out.println(whoAmI+ "Trying to create : " + ct_id+" on "+serverNameToCreateCT);
								int myRad = Math.abs(rndRAM.nextInt())%3;
								long ctMemory = Constants.RAM_SIZE[myRad];
								api.createCT(serverNameToCreateCT, Long.toString(ct_nb), ct_name, ctMemory);

								boolean isStarted = false;
								while(!isStarted) {
									try{
										api.startCT(serverNameToCreateCT, Long.toString(ct_nb));
										isStarted = true;
										System.out.println(whoAmI+ "CT Started, yoohoo");
									}catch (IOException e){
										Thread.sleep(2000);
									}
								}
								// planifier la prochaine cr�ation
								int timeToWait = getNextEventExponential(lambda); // par exemple une loi expo d'une moyenne de 30sec
								
								// attendre jusqu'au prochain �v�nement
								Thread.sleep(timeToWait*2000);

							}
						} else {
							System.out.println("Servers are loaded, waiting 5 more seconds ...");
							Thread.sleep(Constants.GENERATION_WAIT_TIME);
						}
					}catch (IOException ex){
						System.err.println(ex.getMessage()+", renewing attempt...");
					}
					catch (Exception e) {
						e.printStackTrace();
					}
				}

			}
		
		}
	}


