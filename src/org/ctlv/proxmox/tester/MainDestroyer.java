package org.ctlv.proxmox.tester;

import java.io.IOException;
import java.util.List;

import javax.security.auth.login.LoginException;

import org.ctlv.proxmox.api.Constants;
import org.ctlv.proxmox.api.ProxmoxAPI;
import org.ctlv.proxmox.api.data.LXC;
import org.ctlv.proxmox.generator.MyGenerator;
import org.ctlv.proxmox.myManager.Manager;
import org.json.JSONException;

public class MainDestroyer {

    public static void main(String[] args) throws LoginException, JSONException, IOException, InterruptedException {
        ProxmoxAPI myApi= new ProxmoxAPI();
        myApi.login();
        System.out.println("Logged in");
        myApi.startCT(Constants.SERVER1, "21211");
        System.out.println("Starting 21211");
        Thread.sleep(5000);
        System.out.println("Killing 21211...");
        myApi.stopCT(Constants.SERVER1,"21211");
        System.out.println("21211 has stopped.");
        Thread.sleep(5000);
        System.out.println(myApi.getCT(Constants.SERVER1, "21211").getStatus());
        myApi.migrateCT(Constants.SERVER1,"21211", "");
        System.out.println("21211 was killed.");
    }

}