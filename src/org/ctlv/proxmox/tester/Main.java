package org.ctlv.proxmox.tester;

import java.io.IOException;
import java.util.List;

import javax.security.auth.login.LoginException;

import org.ctlv.proxmox.api.ProxmoxAPI;
import org.ctlv.proxmox.api.data.LXC;
import org.ctlv.proxmox.generator.MyGenerator;
import org.ctlv.proxmox.myManager.Manager;
import org.json.JSONException;

public class Main {

	public static void main(String[] args) throws LoginException, JSONException, IOException {
		ProxmoxAPI myApi= new ProxmoxAPI();
		Manager myManager = new Manager();
		try {
			myManager.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			new MyGenerator();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}


		System.out.println("Running ...");

		
	}

}
