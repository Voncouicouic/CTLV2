package org.ctlv.proxmox.myManager;

import org.ctlv.proxmox.api.Constants;
import org.ctlv.proxmox.api.ProxmoxAPI;
import org.ctlv.proxmox.api.data.LXC;
import org.ctlv.proxmox.api.data.Node;
import org.json.JSONException;

import javax.security.auth.login.LoginException;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.ctlv.proxmox.api.Constants.CT_BASE_NAME;
import static org.ctlv.proxmox.api.Constants.SERVER1;
import static org.ctlv.proxmox.api.Constants.SERVER2;

public class Manager extends Thread{
    public ProxmoxAPI api;
    String whoAmI = "[Manager]: ";

    int totalRamUsed=0;
    int totalCPUUsed=0;

    Set<String> inTransit;

    public Manager() {
         this.api = new ProxmoxAPI();
         inTransit=new HashSet<>();
    }

    public void run(){ //TODO: FIX THIS ABOMINATION
        System.out.println(whoAmI+"Starting the manager..");
        while(true) {
            try {

                //Purge the transit list
                List<LXC> myCTs1 = api.getCTs(SERVER1);
                List<LXC> myCTs2 = api.getCTs(SERVER1);
                for (String s : inTransit) {
                    String ctName = s.split(":")[0];
                    String servName = s.split(":")[1];
                    if (servName.equals(SERVER1)) {
                        //TODO
                    }
                }

                float ramUsedByServ1 = getStatusNode(SERVER1);
                float ramUsedByServ2 = getStatusNode(SERVER2);
                manageNodes(ramUsedByServ1, ramUsedByServ2);
                System.out.println(whoAmI+"px8 RAM Load is at "+ramUsedByServ1);
                System.out.println(whoAmI+"px7 RAM Load is at "+ramUsedByServ2);



//                    for (LXC myCT : myCTs1 ) {
//                        if (isOurs(myCT.getName()))
//                            System.out.println(whoAmI + SERVER1 + " " + myCT.getName() + " " + myCT.getStatus() + " " + myCT.getUptime());
//                    }
//                    for (LXC myCT : myCTs2) {
//                        if(isOurs(myCT.getName()))
//                         System.out.println(whoAmI+ SERVER2+" "+myCT.getName() + " " + myCT.getStatus() + " " + myCT.getUptime());
//                    }

                System.out.println(whoAmI+"Sleeping now...");
                Thread.sleep(4000);
            }
            catch (Exception e){

                e.printStackTrace();
            }

        }
    }

    /**
     * This method get the status of the node: Total ram, total ram used, ram used by our cts, etc.
     */

    public float getStatusNode(String nodeName)throws InterruptedException, LoginException, JSONException, IOException{
        Node node = api.getNode(nodeName);
        long memTotalNode = node.getMemory_total();

        List<LXC> myCTs= api.getCTs(nodeName);

        long totalRamUsedByUs = 0;

        for(LXC lxc: myCTs){
            if(isOurs(lxc.getName())){
                totalRamUsedByUs+=lxc.getMem();
            }
        }
        return  totalRamUsedByUs/(float)memTotalNode*100;

    }

    private boolean isOurs(String name) {
       return name.contains(CT_BASE_NAME);
    }

    /**
     * Rough policy to balance load. Could do it more thinly by ranking the ram usage of the cts and offloading just enough cts to meet the thresold
     * requirements
     * @param chargeServ1
     * @param chargeServ2
     */
    private void manageNodes(float chargeServ1, float chargeServ2)throws Exception{



        if(chargeServ1 > 16 || chargeServ2 > 16){
            System.out.println(whoAmI+"Charge exceeding 16%");
            Constants.IS_GENERATOR_ALLOWED_TO_GENERATE = false;
        } else{
            Constants.IS_GENERATOR_ALLOWED_TO_GENERATE = true; //Reallow the generation if neither of the servers are overloaded.
        }
        if(chargeServ1 > 8 && chargeServ2 < 8){

            System.out.println(whoAmI+"Charge exceeding 8% on px8");
            LXC toMigrate = getBiggestCT(SERVER1);
            inTransit.add(toMigrate.getName()+":"+SERVER1);
            api.migrateCT(SERVER1,toMigrate.getName(),SERVER2);
        }else if (chargeServ1 <8 && chargeServ2 > 8){
            System.out.println(whoAmI+"Charge exceeding 8% on px7");
            LXC toMigrate = getBiggestCT(SERVER2);
            inTransit.add(toMigrate.getName()+":"+SERVER2);
            api.migrateCT(SERVER2,toMigrate.getName(),SERVER1);
        } else{
            if(chargeServ1>12){
                System.out.println(whoAmI+"Charge exceeding 12% on px8");

                killOldest(SERVER1);
            }
            if(chargeServ2>12){

                System.out.println(whoAmI+"Charge exceeding 12% on px7");
                killOldest(SERVER2);
            }
        }

    }
    private LXC getBiggestCT(String servName) throws Exception{
        List<LXC> myCTs= api.getCTs(servName);
        float mostRamUsed=0;
        LXC biggestCT = null;
        for(LXC lxc: myCTs){
            if(!inTransit.contains(lxc.getName()+":"+servName)){  //If the ct is already in transit, do not concern with it
             if(lxc.getMem()>mostRamUsed){
                mostRamUsed=lxc.getMem();
                biggestCT=lxc;
             }
            }
        }
        return biggestCT;
    }
    private void killOldest(String servName) throws Exception{
        List<LXC> myCTs= api.getCTs(servName);
        float age=0;
        LXC oldestCT = null;
        for(LXC lxc: myCTs){
            if(lxc.getUptime()>age){
                age=lxc.getUptime();
                oldestCT=lxc;
            }
        }
        api.stopCT(servName,oldestCT.getName());
    }

    private void checkMigrationStatus() {}
}
