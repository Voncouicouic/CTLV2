package org.ctlv.proxmox.myManager;

import org.ctlv.proxmox.api.ProxmoxAPI;
import org.json.JSONException;

import javax.security.auth.login.LoginException;
import java.io.IOException;

public class ManagerMain {

    public ManagerMain(){}
    public static void main(String[] args) throws InterruptedException, LoginException, JSONException, IOException, Exception {
        Manager myManager = new Manager();
        System.out.println("Running ...");
        myManager.run();
    }
}
