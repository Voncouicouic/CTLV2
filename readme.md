# Authors
Max Etter and Leo Le Lonquer from SDBD B1

# How to run
This version is best used by launching the main in the Main class, which will launch 2 threads with a generator and a monitor. The monitor dictates the status of a shared variable allowing the generator to generate or not
# Note:
This version is relying on an API apparently devoid of destroy method for LXCs. So the oldest LXCs will be halted by the monitor instead of deleted.
